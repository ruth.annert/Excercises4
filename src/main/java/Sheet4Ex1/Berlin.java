package Sheet4Ex1;

public class Berlin {

    public static void main(String[] args) {
        String city = "Tallinn";

        if (city == "Milano") {
            System.out.println("Ilm on ilus.");
        } else if (city == "Otepää") {
            System.out.println("Hea suusailm.");
        } else {
            System.out.println("Ilm polegi kõige tähtsam!");
        }
    }
}
