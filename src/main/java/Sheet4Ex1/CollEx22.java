package Sheet4Ex1;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CollEx22 {
    public static void main(String[] args) {
        Map<String, String[]> map = new HashMap<>();
        //Sting tähistab riiki ja linna, riik on key
        String[] estCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        map.put("Estonia", estCities);
        String[] sweCities = {"Stockholm, Uppsala, Lund, Köping"};
        map.put("Sweden", sweCities);
        String[] finCities = {"Helsinki", "Espoo", "Hanko", "Jämsä"};
        map.put("Finland", finCities);

        for (Map.Entry<String, String[]> country : map.entrySet()) {
            //võtab mapist kõik itmeid ja annab ig akord ühe tagasi
            //country asemel võib ükskõik mida printida, defineerime muutuja
            System.out.println("Country:" + country.getKey());
            System.out.println("Cities:");
            for (String city : country.getValue()) {
                System.out.println("\t" + city);
            }
            map.forEach((k, v) -> {
                //k key v value
                System.out.println("Country: " + k);
                System.out.println("Cities:");
                for (String city : v)
                {
                    System.out.println("\t" + city);
                }
            });
            //enesepiinamine, ei tohiks rakendada, nii tehti vanasti
            //Set<String> keys = map.keySet();
            Object[] keys = map.keySet().toArray();
            for (int i = 0; i < map.size(); i++){
                if (keys[i] instanceof String){
                    System.out.println("Ongi teksti tüüp");
                }
                //defineerime ajutise muutuja i, käime läbi üks element korraga
                String key = (String) keys[i];
                System.out.println("Country:" + key);
                System.out.println("Cities:");
                String[] values = map.get(key);
                for (int j = 0; j < map.get(key).length; j++){
                    System.out.println("\t" + values[j]);
                    // /t nihutab nagu tab

                }

            }
        }
    }
}
