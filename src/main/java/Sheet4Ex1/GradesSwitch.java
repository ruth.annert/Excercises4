package Sheet4Ex1;

public class GradesSwitch {
    public static void main(String[] args) {

        int grade = 8;

        switch (grade) {
            case 1:
                System.out.println("nõrk");
                break;
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("väga hea");
                break;
             default:
                 System.out.println("Invalid grade");
        }
    }
}


